import { state } from '@angular/animations';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './../../model/destinos-viajes-state.model';
import { DestinoViaje } from './../..//model/destino-viaje.model';
import { DestinosApiClient } from './../../model/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ DestinosApiClient ]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded:EventEmitter<DestinoViaje>;
  updates: string[];
  all: any;
  constructor(
    public destinosApiClient:DestinosApiClient, 
    private store: Store<AppState>) { 

    this.onItemAdded = new EventEmitter();
    this.updates = [];
    // this.store.select(state => state.destinos.favorito)
    //   .subscribe(d => {
    //     if(d != null){
    //       this.updates.push('Se ha elegido a ' + d.nombre);  
    //     }
    //   });
    //   this.all = store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit(): void {
    this.store.select(state => state.destinos)
    .subscribe(data => {
      let d = data.favorito;
      if (d != null) {
        this.updates.push("Se eligió: " + d.nombre);
      }
    });
  }

  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    // this.store.dispatch(new NuevoDestinoAction(d)); 
  }

  elegido(e: DestinoViaje){
    this.destinosApiClient.elegir(e);
  }

  getAll(){

  }

}
